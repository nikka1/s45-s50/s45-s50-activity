import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout() {

	const {unsetUser, setUser} = useContext(UserContext);
	// localStorage.clear() method allows to clear information in local storage
	// localStorage.clear()
	unsetUser();

	// Add a useEffect to run our setUser. This useEffect will have an empty dependency array.
	useEffect(()=>{
	    setUser({id: null})
	  }, [])

	return(

		<Navigate to="/login" />
		)
}