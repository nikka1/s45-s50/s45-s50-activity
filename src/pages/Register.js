import {useState, useEffect, useContext} from 'react';
import {useNavigate, Navigate} from "react-router-dom";
import Swal from 'sweetalert2';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Register () {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('')
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e) {

		e.preventDefault()

		// ACTION: check if email used in registeration has a duplicate

		fetch('http://localhost:4000/users/checkEmail', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please try another email address"
				})
				
			}

			else {

			// ACTION: register user

				fetch('http://localhost:4000/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
					.then(data => {

						if(data === true) {
							Swal.fire({
								title: "Welcome to Zuitt",
								icon: "success",
								text: "You may now login"
							})

							navigate("/login")

						}

						else {

							Swal.fire({
								title: "Registration failed",
								icon: "error",
								text: "Please try again"
							})
						}

					})

			}

		})

		// setFirstName('');
		// setLastName('');
		// setEmail('');
		// setMobileNo('');
		// setPassword1('');
		// setPassword2('');

		// alert('Thank you for registering');


		/*
			Activity:
			Create a post request using fetch to the /checkEmail endpoint of our API that will check if the email the user is trying to submit is unique or not.

			If email is unique:
			console.log the API response
			

			If email is not unique, show a sweetalert notification saying "Duplicate email found -- Please try another email address"
		*/
	}


	useEffect(() => {
		// no blank fields
		// mobile number must be exactly be 11 characters
		// passwords must match

		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (mobileNo.length === 11 ) && (password1 === password2)) {
			setIsActive(true);
		}
		else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password1, password2])


	return(

		(user.id !== null) ? 
	    
	    <Navigate to ="/courses" />
	    
	    :

		<Form className="mt-3 "onSubmit = {(e) => registerUser(e)}>
		<h1>Registration page</h1>
		  <Form.Group>
		  	<Form.Label>First Name</Form.Label>
		  	<Form.Control
		  		type="text"
				placeholder="Enter first name"
				value = {firstName}
				onChange = {e => setFirstName(e.target.value)}
				required
		  	/>
		  </Form.Group>

		  <Form.Group>
		  	<Form.Label>Last Name</Form.Label>
		  	<Form.Control
		  		type="text"
				placeholder="Enter last name"
				value = {lastName}
				onChange = {e => setLastName(e.target.value)}
				required
		  	/>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control
		    				type="email"
		    				placeholder="Enter email"
		    				value = {email}
		    				onChange = {e => setEmail(e.target.value)}
		    				required />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group>
		  	<Form.Label>Mobile number</Form.Label>
		  	<Form.Control
		  		type="text"
				placeholder="Enter mobile number"
				value = {mobileNo}
				onChange = {e => setMobileNo(e.target.value)}
				required
		  	/>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    				type="password" 
		    				placeholder="Password"
		    				value={password1}
		    				onChange= {e => setPassword1(e.target.value)}
		    				required />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify password</Form.Label>
		    <Form.Control 
		    				type="password" 
		    				placeholder="Verify password"
		    				value={password2}
		    				onChange={e => setPassword2(e.target.value)}
		    				required />
		  </Form.Group>

		  {
		  	isActive ?
		  		<Button variant="primary" type="submit" id="submitBtn">
		    		Submit
		  		</Button>
		  		:
		  		<Button variant="danger" type="submit" id="submitBtn" disabled>
		    		Submit
		  		</Button>
		  }
		</Form>
		)
}