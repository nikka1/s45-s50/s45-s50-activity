import {useState, useEffect} from 'react';
import {Button,Row, Col, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard ({courseProp}) {

	// console.log(props)
	// console.log(typeof props)
	// console.log(courseProp)

	const { name, description, price, _id } = courseProp
	console.log(courseProp);

	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue)
	*/
	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(30)

	// // useState - initial value
	// function enroll () {

	// 			setCount(count + 1)
	// 			console.log('Enrollees:' + count)
	// 			setSeats(seats - 1)
	// 			console.log('Seats:' + seats);

	// 	}

	// 	useEffect(() => {

	// 		if(seats === 0) {
	// 			alert("No more seats available")
	// 		}
	// 	}, [seats])


	return(
		
		<Row>
			<Col>
				<Card>
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Text className="mb-1">Description:</Card.Text>
				    <Card.Text>{description}</Card.Text>
				    <Card.Text  className="mb-1">Price:</Card.Text>
				    <Card.Text>PHP {price}</Card.Text>
				    {/*<Card.Text>Enrollees: {count}</Card.Text>
				    <Card.Text>Seats: {seats}</Card.Text>
				    <Button variant="primary" onClick={enroll}>Enroll</Button>*/}

				    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
		)

}